#ifndef CANVAS_H
#define CANVAS_H

#include "Rectangle.h"
#include "Toolbar.h"
#include "Point.h"

struct Canvas{
    Rectangle area;

    Point points[1000];
    int pCounter;

    Canvas(){
        area = Rectangle(-0.8, 1, 1.8, 1.8, Color(0, 1, 0));
        pCounter = 0;
    }

    void draw(){
        area.draw();

        for (int i = 0; i < pCounter; i++){
            points[i].draw();
        }
    }

    void handleMouseClick(float x, float y, Tool tool){
        if (tool == PENCIL){
            points[pCounter] = Point(x, y, Color(0,0,0));
            pCounter++;
        }
    }

    bool contains(float x, float y){
        return area.contains(x, y);
    }
};

#endif