#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "AppController.h"
#include <GL/gl.h>
#include "Toolbar.h"
#include "Canvas.h"


struct Controller : public AppController {
    Toolbar toolbar;
    Canvas canvas;

    Controller(){
        // init toolbar...
    }

    void leftMouseDown( float x, float y ){
        if (toolbar.contains(x, y)){
            toolbar.handleMouseClick(x, y);
        }
        else if (canvas.contains(x, y)){
            canvas.handleMouseClick(x, y, toolbar.selectedTool);
        }
    }

    void render(){
        // display toolbar
        toolbar.draw();
        canvas.draw();
    }
};

#endif